Categories = new Mongo.Collection('categories');
Games = new Mongo.Collection('games');
if (Meteor.isClient) {
  var game = null;
  function Game (category, userName) {
    //Category of the game
    this.category = category;
    this.userName = userName;
    this.score = 0;
    this.lastWord = null;

    this.clock = {
      initial:5, //initial: original game duration
      from : 5, //From: game duration
      interval : null 
    };

    this.modal = {
      id : 'myModal',
      interval: null
    }

    this.updateTimer=function(){
      Session.set('time', this.clock.from);
    };
    
    this.start=function(){
      $('body').addClass('padding-top');
      Session.set('gameStarted', true);
      Session.set('score', 0);
      this.lastWord = Categories.findOne({text:game.category}).words[0];
      Session.set('wordSequence', this.lastWord);
      this.clock.interval = Meteor.setInterval(this.tick, 1000);
    };
    
    this.tick=function(){
      if (game.clock.from > 0){
        game.clock.from--;
      }else{
        game.end();
      }
      game.updateTimer()
    };

    this.end=function(){
        Meteor.clearInterval(this.clock.interval);
        this.saveGame();
        Session.set('gameStarted', null);
        Session.set('wordSequence', null);
        Session.set('score', 0);
        $('body').removeClass('padding-top');
        this.gameOver();
    };

    this.saveGame=function(){
      Games.insert({
        userName: this.userName,
        score: this.score,
        category: this.category,
        createdAt: new Date()
      });
    };

    this.wordChain=function(word){
      return this.lastWord[this.lastWord.length-1].toLowerCase() == word[0].toLowerCase();
    }

    this.plus=function(word){
      this.score++;
      this.lastWord = word;
      Session.set('wordSequence', Session.get('wordSequence') +' '+ word);
      this.showPoints();
      Session.set('score', this.score);
    }
    this.hint=function(){
      this.showHint();
    }

    this.gameOver=function(){
      Session.set('modaltext','Game Over');
      this.showModal(2000);
    }

    this.showPoints=function(){
      Session.set('modaltext','You have +1 points! Well done!');
      this.showModal(1000);
    }

    this.showHint=function(){
      Session.set('modaltext','Remember, word must start with '+ this.lastWord[this.lastWord.length-1]);
      $('.modal-content').addClass('error-modal');
      this.showModal(2000);
    }

    this.showModal=function(time){
      $('#'+this.modal.id).modal('show');
      this.modal.interval = Meteor.setInterval(this.hideModal, time);
    }

    this.hideModal=function(){
      Session.set('modaltext', null);
      $('.modal-content').removeClass('error-modal');
      $('#'+game.modal.id).modal('hide');
      Meteor.clearInterval(game.modal.interval);
    }

  };

  Template.body.helpers({
    categories: function () {
      return Categories.find({});
    },
    gameStarted:function(){
      return Session.get('gameStarted');
    },
    games:function(){
      return Games.find({},{sort: {score: -1}})
    },
    modaltext:function(){
      return Session.get('modaltext');
    }
  });
  
  Template.timer.helpers({
    time:function(){
      return Session.get('time');
    } 
  });
  
  Template.scoring.helpers({
    score:function(){
        return Session.get('score'); 
    },
  });

  Template.words.helpers({
    word:function(){
        return Session.get('wordSequence'); 
    },
  });
  Template.picklist.helpers({
      list:function(){
        return Categories.findOne({text:game.category}).words; 
    },
  });

  Template.stats.helpers({
    list:function(){
      return Games.find({sort:'score'})
    }
  });

  Template.picklist.events({
    'click .pick':function(){
      var addClass = 'label-danger';
      if (game.wordChain(this)){
          game.plus(this);
          addClass = 'label-success';
      }else{
          game.hint();    
      }
      $('.pick-list').find('[data-id=\''+this+'\']').removeClass('pick');
      $('.pick-list').find('[data-id=\''+this+'\']').addClass(addClass);
    },

  
  });

  Template.body.events({
    'click .start': function (event) { 
      var category = $('.new-player').find('select').find(':selected').text();
      var userName = $('.new-player').find('.name').val();
      // Start game
      game = new Game(category, userName);
      game.start();
    }
  });
}

if (Meteor.isServer) {
  Meteor.startup(function () {
    Categories.remove({});
    Categories.insert({ text: "Food", words: ['acorn squash','alfalfa sprouts','almond','anchovy','anise','appetizer','appetite','apple','apricot','artichoke','asparagus','aspic','ate','avocado','bacon','bagel','bake','baked Alaska','bamboo shoots','banana','barbecue','barley','basil','batter','beancurd','beans','beef','beet','bell pepper','berry','biscuit','bitter','black  beans','blackberry','black-eyed peas','black tea','bland','blood orange','blueberry','boil','bowl','boysenberry','bran','bread','breadfruit','breakfast','brisket','broccoli','broil','brownie','brown rice','brunch','Brussels sprouts','buckwheat','buns','burrito','butter','butter bean','cake','calorie','candy','candy apple','cantaloupe','capers','caramel','caramel apple','carbohydrate','carrot','cashew','cassava','casserole','cater','cauliflower','caviar','cayenne pepper','celery','cereal','chard','cheddar','cheese','cheesecake','chef','cherry','chew','chicken','chick peas','chili','chips','chives','chocolate','chopsticks','chow','chutney','cilantro','cinnamon','citron','citrus','clam','cloves','cobbler','coconut','cod','coffee','coleslaw','collard greens','comestibles','cook','cookbook','cookie','corn','cornflakes','cornmeal','cottage cheese','crab','crackers','cranberry','cream','cream cheese','crepe','crisp','crunch','crust','cucumber','cuisine','cupboard','cupcake','curds','currants','curry','custard','daikon','daily bread','dairy','dandelion greens','Danish pastry','dates','dessert','diet','digest','digestive system','dill','dine','diner','dinner','dip','dish','dough','doughnut','dragonfruit','dressing','dried','drink','dry','durian','eat','Edam cheese','edible','egg','eggplant','elderberry','endive','entree','fast','fat','fava bans','feast','fed','feed','fennel','fig','fillet','fire','fish','flan','flax','flour','food','food pyramid','foodstuffs','fork','freezer','French fries','fried','fritter','frosting','fruit','fry','garlic','gastronomy','gelatin','ginger','gingerale','gingerbread','glasses','Gouda cheese','grain','granola','grape','grapefruit','grated','gravy','greenbean','greens','green tea','grub','guacamole','guava','gyro','herbs','halibut','ham','hamburger','hash','hazelnut','herbs','honey','honeydew','horseradish','hot','hot dog','hot sauce','hummus','hunger','hungry','ice','iceberg lettuce','iced tea','icing','ice cream','ice cream cone','jackfruit','jalapeno','jam','jelly','jellybeans','jicama','jimmies','Jordan almonds','jug','julienne','juice','junk food','kale','kebab','ketchup','kettle','kettle corn','kidney beans','kitchen','kiwi','knife','kohlrabi','kumquat','ladle','lamb','lard','lasagna','legumes','lemon','lemonade','lentils','lettuce','licorice','lima beans','lime','liver','loaf','lobster','lollipop','loquat','lox','lunch','lunch box','lunchmeat','lychee','macaroni','macaroon','main course','maize','mandarin orange','mango','maple syrup','margarine','marionberry','marmalade','marshmallow','mashed potatoes','mayonnaise','meat','meatball','meatloaf','melon','menu','meringue','micronutrient','milk','milkshake','millet','mincemeat','minerals','mint','mints','mochi','molasses','mole sauce','mozzarella','muffin','mug','munch','mushroom','mussels','mustard','mustard greens','mutton','napkin','nectar','nectarine','nibble','noodles','nosh','nourish','nourishment','nut','nutmeg','nutrient','nutrition','nutritious','oats','oatmeal','oil','okra','oleo','olive','omelet','omnivore','onion','orange','order','oregano','oven','oyster','pan','pancake','papaya','parsley','parsnip','pasta','pastry','pate','patty','pattypan squash','peach','peanut','peanut butter','pea','pear','pecan','peapod','pepper','pepperoni','persimmon','pickle','picnic','pie','pilaf','pineapple','pita bread','pitcher','pizza','plate','platter','plum','poached','pomegranate','pomelo','pop','popsicle','popcorn','popovers','pork','pork chops','pot','potato','pot roast','preserves','pretzel','prime rib','protein','provisions','prune','pudding','pumpernickel','pumpkin','punch','quiche','quinoa','radish','raisin','raspberry','rations','ravioli','recipe','refreshments','refrigerator','relish','restaurant','rhubarb','ribs','rice','roast','roll','rolling pin','romaine','rosemary','rye','saffron','sage','salad','salami','salmon','salsa','salt','sandwich','sauce','sauerkraut','sausage','savory','scallops','scrambled','seaweed','seeds','sesame seed','shallots','sherbet','shish kebab','shrimp','slaw','slice','smoked','snack','soda','soda bread','sole','sorbet','sorghum','sorrel','soup','sour','sour cream','soy','soybeans','soysauce','spaghetti','spareribs','spatula','spices','spicy','spinach','split peas','spoon','spork','sprinkles','sprouts','spuds','squash','squid','steak','stew','stir-fry','stomach','stove','straw','strawberry','string bean','stringy','strudel','sub sandwich','submarine sandwich','succotash','suet','sugar','summer squash','sundae','sunflower','supper','sushi','sustenance','sweet','sweet potato','Swiss chard','syrup','taco','take-out','tamale','tangerine','tapioca','taro','tarragon','tart','tea','teapot','teriyaki','thyme','toast','toaster','toffee','tofu','tomatillo','tomato','torte','tortilla','tuber','tuna','turkey','turmeric','turnip','ugli fruit','unleavened','utensils','vanilla','veal','vegetable','venison','vinegar','vitamin','wafer','waffle','walnut','wasabi','water','water chestnut','watercress','watermelon','wheat','whey','whipped cream','wok','yam','yeast','yogurt','yolk','zucchini']});
    Categories.insert({ text: "Animals", words:['aardvark', 'abalone', 'African gray parrot', 'African penguin', 'African elephant', 'African rock python', 'African wild cat', 'albatross', 'algae', 'agouti', 'airedale terrier', 'Alaskan malamute', 'alligator', 'alpaca', 'amoeba', 'American bison', 'American cocker spaniel', 'American crocodile', 'American flamingo', 'American golden plover', 'American Robin', 'American tree sparrow', 'amphibian', 'anaconda', 'angelfish', 'angelshark', 'angonoka', 'animal', 'anole', 'ant', 'anteater', 'antelope', 'Apatosaurus', 'ape', 'aphid', 'arachnid', 'archaeopteryx', 'arctic fox', 'Arctic tern', 'arctic wolf', 'armadillo', 'Arsinoitherium', 'arthropod', 'artiodactyls', 'asp', 'assassin bug', 'aye-aye', 'baboon', 'bactrian camel', 'badger', 'bald eagle', 'bandicoot', 'barnacle', 'barracuda', 'basilisk', 'basking shark', 'bass', 'basset hound', 'bat', 'beagle', 'bear', 'bearded dragon', 'beaver', 'bed bug', 'bee', 'beetle', 'beluga whale', 'bichon frise', 'bighorn sheep', 'bilby', 'binturong', 'bird', 'bison', 'bivalve', 'black bear', 'black bear hamster', 'blackbird', 'black caiman', 'black racer', 'black swan', 'bloodhound', 'blowfish', 'bluebird', 'bluefin tuna', 'blue jay', 'blue morpho butterfly', 'blue ring octopus', 'blue shark', 'blue-tongued skink', 'blue whale', 'boa constrictor', 'bobcat', 'bongo', 'bonobo', 'bony fish', 'border collie', 'Boston terrier', 'bowhead whale', 'boxer', 'brittle star', 'brown bear', 'brown pelican', 'box turtle', 'brittle star', 'bug', 'buffalo', 'bull', 'bulldog', 'bullfrog', 'bull shark', 'bull snake', 'bumblebee', 'bushbaby', 'butterfly', 'caiman', 'California sea lion', 'camel', 'canary', 'cape buffalo', 'capybera', 'Canada goose', 'Cape hunting dog', 'caracal', 'cardinal', 'caribou', 'carnivora', 'cassowary', 'carpenter ant', 'cat', 'catamount', 'caterpillar', 'cattle', 'cavy', 'centipede', 'cephalpod', 'chameleon', 'cheetah', 'chickadee', 'chicken', 'chihuahua', 'chimipanzee', 'chinchilla', 'chipmunk', 'chiton', 'chrysalis', 'cicada', 'clam', 'clownfish', 'coati', 'cobra', 'cockatoo', 'cod', 'coelacanth', 'cockroach', 'collared lizard', 'collared peccary', 'collie', 'colugo', 'common rhea', 'companion dog', 'conch', 'cookiecutter shark', 'copepod', 'copperhead snake', 'coral', 'coral snake', 'corn snake', 'cottonmouth', 'cougar', 'cow', 'coyote', 'coypu', 'crab', 'crane', 'crayfish', 'cricket', 'crocodile', 'crow', 'crustacean', 'Cryptoclidus', 'cuttlefish', 'cutworm', 'Dachshund', 'dall sheep', 'Dall\'s porpoise', 'Dalmatian', 'dark-eyed junco', 'damselfly ', 'darkling beetle', 'deer', 'Deinonychus', 'desert tortoise', 'Desmatosuchus', 'dhole', 'diatom', 'Dilophosaurus', 'Dimetrodon', 'Dinichthys', 'dingo', 'Dinornis', 'dinosaur', 'Diplodocus', 'Doberman pinscher', 'dodo', 'Doedicurus', 'dog', 'dogfish', 'dolphin', 'dolphin, bottlenose', 'dolphin, spotted', 'donkey', 'dory', 'dove', 'downy woodpecker', 'dragonfly', 'dromedary', 'duck', 'dugong', 'duck-billed platypus', 'dugong', 'dung beetle', 'Dunkleosteus', 'eagle', 'earthworm', 'earwig', 'eastern bluebird', 'eastern quoll', 'echidna', 'echinoderms', 'Edenta', 'Edmontonia', 'Edmontosaurus', 'eel', 'egg', 'egret', 'ekaltadelta', 'eland', 'Elasmosaurus', 'Elasmotherium', 'electric eel', 'elephant', 'elephant seal', 'elk', 'emerald tree boa', 'emperor angelfish', 'emperor penguin', 'emu', 'endangered species', 'Eohippus', 'Eoraptor', 'ermine', 'Estemmenosuchus', 'extinct animals', 'Fabrosaurus', 'falcon', 'farm animals', 'fennec fox', 'ferret', 'fiddler crab', 'finch', 'fin whale', 'fireant', 'firefly', 'fish', 'flamingo', 'flatworm', 'flea', 'flightless birds', 'flounder', 'fly', 'flying fish', 'flying squirrel', 'forest antelope', 'forest giraffe', 'fossa', 'fowl', 'fox', 'frilled lizard', 'frog', 'fruit bat', 'fruit fly', 'fugu', 'galagos', 'Galapagos shark', 'gar', 'gastropod', 'gavial', 'gazelle', 'gecko', 'gerbil', 'German shepherd', 'giant squid', 'gibbon', 'gila monster', 'giraffe', 'Glyptodon', 'gnat', 'gnu', 'goat', 'golden eagle', 'golden lion tamarin', 'golden retriever', 'goldfinch', 'goldfish', 'goose', 'gopher', 'gorilla', 'grasshopper', 'gray whale', 'great apes', 'great Dane', 'great egret', 'great horned owl', 'great white shark', 'green darner dragonfly', 'green iguana', 'Greenland shark', 'greyhound', 'grizzly bear', 'groundhog', 'grouper', 'grouse', 'grub', 'guinea pig', 'gull', 'gulper eel', 'hammerhead shark', 'hamster', 'hare', 'harlequin bug', 'harp seal', 'harpy eagle', 'hatchetfish', 'Hawaiian goose', 'hawk', 'hedgehog', 'hen', 'hermit crab', 'heron', 'herring', 'hippo', 'hippopotamus', 'honey bee', 'hornet', 'horse', 'horseshoe crab', 'hound', 'house fly', 'howler monkey', 'human being', 'hummingbird', 'humpback whale', 'husky', 'hyena', 'Hyracotherium', 'hyrax', 'ibis', 'Ichthyornis', 'Ichthyosaurus', 'iguana', 'Iguanodon', 'imago', 'impala', 'Indian elephant', 'insect', 'insectivores', 'invertebrates', 'Irish setter', 'isopod', 'jack rabbit', 'Jack Russell terrier', 'jaguar', 'Janenschia', 'Japanese crane', 'javelina', 'jay', 'jellyfish', 'jerboa', 'joey', 'John Dory', 'jumping bean moth', 'junebug', 'junco', 'kakapo', 'kangaroo', 'kangaroo rat', 'karakul', 'katydid', 'keel-billed toucan', 'Kentrosaurus', 'killer whale', 'king cobra', 'king crab', 'kinkajou', 'kiwi', 'knobbed whelk', 'koala', 'Komodo dragon', 'kookaburra', 'krill', 'Kronosaurus', 'Kudu', 'Labrador retriever', 'ladybug', 'lagomorph', 'lake trout', 'lanternfish', 'larva', 'leafcutter ant', 'leghorn', 'lemming', 'lemon shark', 'lemur', 'leopard', 'Lhasa apso', 'lice', 'lightning bug', 'limpet', 'lion', 'Liopleurodon', 'lizard', 'llama', 'lobster', 'locust', 'loggerhead turtle', 'longhorn', 'loon', 'lorikeet', 'loris', 'louse', 'luminous shark', 'luna moth', 'lynx', 'macaque', 'macaw', 'mackerel', 'Macrauchenia', 'maggot', 'mako shark', 'mallard duck', 'mammal', 'mammoth', 'manatee', 'mandrill', 'mamba', 'man-o\'-war', 'manta ray', 'mantid', 'mantis', 'marbled murrelet', 'marine mammals', 'marmoset', 'marmot', 'marsupial', 'mastiff', 'mastodon', 'meadowlark', 'mealworm', 'meerkat', 'Megalodon', 'megamouth shark', 'merganser', 'midge', 'migrate', 'millipede', 'mink', 'minnow', 'mice', 'mockingbird', 'moa', 'mockingbird', 'mole', 'mollusk', 'monarch butterfly', 'mongoose', 'monitor lizard', 'monkey', 'monotreme', 'moose', 'moray eel', 'Morganucodon', 'morpho butterfly', 'mosquito', 'moth', 'mountain lion', 'mouse', 'mudpuppy', 'musk ox', 'muskrat', 'mussels', 'mustelids', 'nabarlek', 'naked mole-rat', 'nandu', 'narwhal', 'nautilus', 'nene', 'nest', 'newt', 'nightingale', 'nine-banded armadillo', 'North American beaver', 'North American porcupine', 'northern cardinal', 'northern elephant seal', 'northern fur seal', 'northern right whale', 'numbat', 'nurse shark', 'nuthatch', 'nutria', 'nymph', 'ocelot', 'octopus', 'okapi', 'old English sheepdog', 'onager', 'opossum', 'orangutan', 'orca', 'Oregon silverspot butterfly', 'oriole', 'Ornitholestes', 'Ornithomimus', 'oropendola', 'Orthacanthus', 'oryx', 'ostrich', 'otter, river', 'otter, sea', 'Oviraptor', 'owl', 'ox', 'oxpecker', 'oyster', 'painted lady butterfly', 'painted turtle', 'panda', 'pangolin', 'panther', 'parakeet', 'parrot', 'peacock', 'peafowl', 'pekingese', 'pelican', 'penguin', 'peregrine falcon', 'Perissodactyls', 'petrel', 'pig', 'pigeon', 'pika', 'pill bug', 'pinnipeds', 'piranha', 'placental mammals', 'plankton', 'platybelodon', 'platypus', 'ploughshare tortoise', 'plover', 'polar bear', 'polliwog', 'pomeranian', 'pompano', 'pond skater', 'poodle', 'porcupine', 'porpoise', 'Port Jackson shark', 'Portuguese water dog', 'Postosuchus', 'prairie chicken', 'praying mantid', 'praying mantis', 'primates', 'Proboscideans', 'pronghorn', 'protozoan', 'pufferfish', 'puffin', 'pug', 'puma', 'pupa', 'pupfish', 'python', 'Quaesitosaurus', 'quagga', 'quail', 'Queen Alexandra\'s birdwing', 'queen conch', 'quetzal', 'quokka', 'quoll', 'rabbit', 'raccoon', 'rat', 'rattlesnake', 'ray', 'redbilled oxpecker', 'red hooded duck', 'red-tailed hawk', 'red kangaroo', 'red panda', 'red wolf', 'reindeer', 'reptile', 'rhea', 'rhino', 'rhinoceros', 'Rhode Island red', 'right whale', 'ring-billed gull', 'ring-tailed lemur', 'ringtail possum', 'river otter', 'roadrunner', 'roach', 'robin', 'rock dove', 'rockhopper penguin', 'rodent', 'rooster', 'rottweiler', 'roundworm', 'ruby-throated hummingbird', 'sailfish', 'salamander', 'salmon', 'sand dollar', 'sandpiper', 'scallop', 'scarlet macaw', 'scorpion', 'Scottish terrier', 'sea anemone', 'sea cow', 'sea cucumber', 'sealion', 'sea cucumber', 'seahorse', 'seal', 'sea otter', 'sea star', 'sea turtle', 'sea urchin', 'sea worm', 'serval', 'shark', 'sheep', 'shrew', 'shrimp', 'siamang', 'Siberian husky', 'silkworm', 'silverfish', 'skink', 'skipper', 'skunk', 'sloth', 'slow worm', 'slug', 'Smilodon', 'snail', 'snake', 'snapper', 'snapping turtle', 'snow goose', 'snow leopard', 'snowy owl', 'softshell turtle', 'sparrow', 'spectacled caiman', 'spectacled porpoise', 'spider', 'spiny anteater', 'spiny lizard', 'sponge', 'spotted owl', 'springtail', 'squid', 'squirrel', 'starfish', 'starling', 'St. Bernard', 'Stegosaurus', 'stingray', 'stonefly', 'stork', 'sugar glider', 'sunfish', 'swallowtail butterfly', 'swan', 'swift', 'swordfish', 'tadpole', 'tamarin', 'tanager', 'tapir', 'tarantula', 'tarpon', 'tarsier', 'Tasmanian devil', 'Tasmanian tiger', 'termite', 'tern', 'terrier', 'Teratosaurus', 'Thecodontosaurus', 'Thescelosaurus', 'three-toed sloth', 'thresher shark', 'thrip', 'tick', 'tiger', 'tiger shark', 'tiger swallowtail butterfly', 'toad', 'Torosaurus', 'tortoise', 'toucan', 'Trachodon', 'treefrog', 'tree shrew', 'tree sparrow', 'Triceratops', 'Trilobite', 'Troodon', 'trout', 'trumpeter swan', 'tuatara', 'tsetse fly', 'tuna', 'tundra wolf', 'turkey', 'turtle', 'T. rex', 'Tyrannosaurus rex', 'Ultrasaurus', 'Ulysses butterfly', 'umbrellabird', 'ungulates', 'uniramians', 'urchin', 'Utahraptor', 'valley quail', 'vampire bat', 'veiled chameleon', 'Velociraptor', 'venomous animals', 'vertebrates', 'viceroy butterfly', 'vinegarroon', 'viper', 'Virginia opossum', 'Vulcanodon', 'vulture', 'walkingstick', 'wallaby', 'walrus', 'warthog', 'wasp', 'waterbug', 'water moccasin', 'water strider', 'weasel', 'Weddell seal', 'weevil', 'western meadowlark', 'western spotted owl', 'west highland white terrier', 'whale', 'whale shark', 'whelk', 'whippet', 'whip scorpion', 'white Bengal tiger', 'white-breasted nuthatch', 'white dove', 'white pelican', 'white rhinoceros', 'white-spotted dolphin', 'white-tailed deer', 'white tiger', 'wild cat', 'wild dog', 'wildebeest', 'wolf', 'wolverine', 'wombat', 'woodchuck', 'woodland caribou', 'wood louse', 'woodpecker', 'woolly bear caterpillar', 'woolly mammoth', 'woolly rhinoceros', 'working dog', 'worm', 'wren', 'Xenarthra', 'xenops', 'Xiaosaurus', 'yak', 'yellowjacket', 'yellow mealworm', 'yellow mongoose', 'Yorkshire terrier', 'zebra', 'zebra bullhead shark', 'zebra longwing butterfly', 'zebra swallowtail butterfly', 'zooplankton', 'zorilla', 'zorro']});
  });
}
